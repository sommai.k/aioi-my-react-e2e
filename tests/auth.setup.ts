import { test as setup, expect } from '@playwright/test';

const adminFile = 'playwright/.auth/admin.json';

setup('admin authenticate', async ({ page }) => {
    await page.goto('/login');
    await page.getByTestId('user-input').fill('admin');
    await page.getByTestId('password-input').fill('12345678');
    await page.getByTestId('login-btn').click();
    await page.waitForURL('/home');
    expect(page.url()).toMatch("/home");

    // End of authentication steps.
    await page.context().storageState({ path: adminFile });
});

const userFile = 'playwright/.auth/user.json';

setup('user authenticate', async ({ page }) => {
    await page.goto('/login');
    await page.getByTestId('user-input').fill('super');
    await page.getByTestId('password-input').fill('12345678');
    await page.getByTestId('login-btn').click();
    await page.waitForURL('/home');
    expect(page.url()).toMatch("/home");

    // End of authentication steps.
    await page.context().storageState({ path: userFile });
});