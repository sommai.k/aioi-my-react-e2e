import { test, expect } from '@playwright/test';

test.describe('admin home', () => {

    test.use({ storageState: 'playwright/.auth/admin.json' });

    test('Home page', async ({ page }) => {
        page.goto('/home');
        await page.getByRole('link', { name: 'Home' }).click();
        await expect(page.locator('#root')).toContainText('Home');
        await expect(page.locator('#root')).toContainText('User Setup');
    });

});

test.describe('user home', () => {

    test.use({ storageState: 'playwright/.auth/user.json' });

    test('Home page', async ({ page }) => {
        page.goto('/home');
        await page.getByRole('link', { name: 'Home' }).click();
        await expect(page.locator('#root')).toContainText('Home');
        await expect(page.locator('#root')).toContainText('User Setup');
    });

});