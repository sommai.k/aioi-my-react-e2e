import { test, expect } from '@playwright/test';

test.use({ storageState: { cookies: [], origins: [] } });

test('Login page', async ({ page }) => {
  await page.goto('/login');
  await expect(page.getByText('Welcome to system')).toBeVisible();
  await expect(page.locator('#root')).toContainText('Welcome to system');
  await expect(page.getByTestId('user-input')).toBeEmpty();
  await expect(page.getByTestId('password-input')).toBeEmpty();
  await expect(page.getByTestId('user-label')).toContainText('Username');
  await expect(page.getByTestId('password-label')).toContainText('Password');
  await expect(page.getByTestId('login-btn')).toContainText('Login');
  await expect(page.getByTestId('reset-btn')).toContainText('Reset');
});

test('Reset form', async ({ page }) => {
  await page.goto('/login');
  await page.getByTestId('user-input').click();
  await page.getByTestId('user-input').fill('abcdefg');
  await page.getByTestId('user-input').press('Tab');
  await page.getByTestId('password-input').fill('abcdefg');
  await page.getByTestId('reset-btn').click();
  await expect(page.getByTestId('user-input')).toBeEmpty();
  await expect(page.getByTestId('password-input')).toBeEmpty();
});

test('Login success', async ({ page }) => {
  await page.goto('/login');
  await page.getByTestId('user-input').click();
  await page.getByTestId('user-input').fill('admin');
  await page.getByTestId('user-input').press('Tab');
  await page.getByTestId('password-input').fill('12345678');
  await page.getByTestId('login-btn').click();
  await page.waitForURL('/home');
  expect(page.url()).toMatch("/home");
});

test('Username and password is required', async ({ page }) => {
  await page.goto('/login');
  await page.getByTestId('login-btn').click();
  await expect(page.getByTestId('user-error')).toContainText('Username is required');
  await expect(page.getByTestId('password-error')).toContainText('Password is required');
  await page.getByTestId('user-input').fill('12345');
  await page.getByTestId('login-btn').click();
  await expect(page.getByTestId('password-error')).toContainText('Password is required');
  await page.getByTestId('reset-btn').click();
  await page.getByTestId('password-input').fill('12345678');
  await page.getByTestId('login-btn').click();
  await expect(page.getByTestId('user-error')).toContainText('Username is required');
});

test('Login fail', async ({ page }) => {
  await page.goto('/login');
  await page.getByTestId('user-input').fill('12345');
  await page.getByTestId('password-input').fill('12345678');
  await page.getByTestId('login-btn').click();
  await expect(page.getByTestId('alert-title')).toContainText('Warning');
});