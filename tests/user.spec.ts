import { test, expect } from '@playwright/test';

test.use({ storageState: 'playwright/.auth/admin.json' });

test('test', async ({ page }) => {
    await page.goto('/user');
    await page.getByTestId('add-btn').click();
    await page.locator('.h-8 > path').click();
});